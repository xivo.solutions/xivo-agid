#!/bin/bash

MTA=msmtp
MTA_CONFIG=/etc/msmtprc
MAIL_CLIENT=mutt
MAIL_CLIENT_CONFIG=/root/.muttrc

is_installed() {
  if ! command -v "$1" $> /dev/null
  then
    echo "=============ERROR============="
    echo "$1 was not installed!"
    echo "==============================="
  fi
}

is_mail_client_configured(){
  [ -f "$MAIL_CLIENT_CONFIG" ]
}

is_mta_configured(){
  [ -f "$MTA_CONFIG" ]
}

configure_mail_client(){
  if ! is_mail_client_configured
  then
    echo "set sendmail=$(command -v $MTA)" > "$MAIL_CLIENT_CONFIG"
  else
    echo "Mail client is configured"
  fi
}

configure_mta(){
  if ! is_mta_configured
  then
    echo "Configuring MTA..."
    echo "defaults
    tls off

    account default
    host xivo
    port 25
    from xivo" >> "$MTA_CONFIG"
  else
    echo "MTA is configured."
  fi
}

is_installed $MTA
is_installed $MAIL_CLIENT
configure_mta
configure_mail_client
