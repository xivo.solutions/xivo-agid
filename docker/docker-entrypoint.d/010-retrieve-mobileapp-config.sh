#!/bin/sh
MAX_RETRIES=6
RETRY_INTERVAL=15

OUTPUT_FILE_FIREBASE=/etc/xivo/xivo-mobile-firebase-adminsdk.json
OUTPUT_FILE_APNS=/etc/xivo/apns/xivo-mobile-apns.pem

entrypoint_log() {
    if [ -z "${AGID_ENTRYPOINT_QUIET_LOGS:-}" ]; then
        echo "$@"
    fi
}

ME=$(basename "$0")

get_firebase_file_from_configmgt() {
    curl "https://xivo/configmgt/api/2.0/mobile/push/config/android" \
        -H 'Accept: application/json' \
        -H "X-Auth-Token: ${PLAY_AUTH_TOKEN}" \
        --insecure \
        --silent \
        -o $OUTPUT_FILE_FIREBASE
}

get_apns_file_from_configmgt(){
  curl "https://xivo/configmgt/api/2.0/mobile/push/config/ios" \
    -H 'Accept: application/json' \
    -H "X-Auth-Token: ${PLAY_AUTH_TOKEN}" \
    --insecure | jq --raw-output '.config' > $OUTPUT_FILE_APNS
}

set_firebase_conf() {
    retry_count=0
    while [ "$retry_count" -lt "$MAX_RETRIES" ]; do
        output_code=$(curl "https://xivo/configmgt/api/2.0/mobile/push/check/android" \
            -H 'Accept: application/json' \
            -H "X-Auth-Token: ${PLAY_AUTH_TOKEN}" \
            --insecure \
            --silent \
            --connect-timeout 5 \
            --max-time 3 \
            -o /dev/null \
            -w "%{http_code}")

        if [ "$output_code" -eq 204 ]; then
            entrypoint_log "$ME: info: Copy firebase token from configmgt"
            get_firebase_file_from_configmgt
            break
        elif [ "$output_code" -eq 404 ]; then
            entrypoint_log "$ME: info: This server is not configured with a firebase push token"
            break
        else
            entrypoint_log "$ME: info: Unable to get firebase from configmgt (Attempt $((retry_count+1)) of $MAX_RETRIES)"
            retry_count=$((retry_count+1))
            if [ "$retry_count" -lt "$MAX_RETRIES" ]; then
                entrypoint_log "$ME: info: Retrying in $RETRY_INTERVAL seconds..."
                sleep "$RETRY_INTERVAL"
            fi
        fi
    done
}

set_apns_conf() {
    retry_count=0
    while [ "$retry_count" -lt "$MAX_RETRIES" ]; do
        output_code=$(curl "https://xivo/configmgt/api/2.0/mobile/push/check/ios" \
            -H 'Accept: application/json' \
            -H "X-Auth-Token: ${PLAY_AUTH_TOKEN}" \
            --insecure \
            --silent \
            --connect-timeout 5 \
            --max-time 3 \
            -o /dev/null \
            -w "%{http_code}")

        if [ "$output_code" -eq 204 ]; then
            entrypoint_log "$ME: info: Copy ios token from configmgt"
            get_apns_file_from_configmgt
            break
        elif [ "$output_code" -eq 404 ]; then
            entrypoint_log "$ME: info: This server is not configured with an APNS push token"
            break
        else
            entrypoint_log "$ME: info: Unable to get APNS from configmgt (Attempt $((retry_count+1)) of $MAX_RETRIES)"
            retry_count=$((retry_count+1))
            if [ "$retry_count" -lt "$MAX_RETRIES" ]; then
                entrypoint_log "$ME: info: Retrying in $RETRY_INTERVAL seconds..."
                sleep "$RETRY_INTERVAL"
            fi
        fi
    done
}

set_firebase_conf
set_apns_conf
