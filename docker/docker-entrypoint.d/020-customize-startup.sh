#!/bin/sh
entrypoint_log() {
    if [ -z "${AGID_ENTRYPOINT_QUIET_LOGS:-}" ]; then
        echo "$@"
    fi
}

ME=$(basename "$0")

start_mds_mode() {
    entrypoint_log "$ME: info: Activate special config for mds"
    if [ -f "/etc/xivo-agid/conf.d/010-mds.yml" ]; then
        entrypoint_log "$ME: info: YML file already exist, skipping moving file"
    else
        mv /etc/xivo-agid/conf.d/010-mds.yml.tmp /etc/xivo-agid/conf.d/010-mds.yml
    fi
}

if [ -n "$IS_MDS" ]; then
    entrypoint_log "$ME: info: Starting in MDS Mode"
    start_mds_mode
else
    entrypoint_log "$ME: info: Starting in Main Mode"
fi

