#!/usr/bin/python3

import socket
import sys


TCP_IP = '127.0.0.1'
TCP_PORT = 4573
BUFFER_SIZE = 1024
MESSAGE = str.encode("""agi_network_script: monitoring
        
        200 result=1
        """)
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((TCP_IP, TCP_PORT))
s.send(MESSAGE)
data = s.recv(BUFFER_SIZE)
s.close()

print("received data:", data.decode().strip().splitlines()[0])
if data.decode().strip().splitlines()[0] == "Status: OK":
    sys.exit(0)
else:
    sys.exit(1)
