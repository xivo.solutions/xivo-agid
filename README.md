# xivo-agid

xivo-agid is a server used by [XiVO](http://xivo.solutions) to serve [AGI](https://wiki.asterisk.org/wiki/pages/viewpage.action?pageId=32375589) requests coming from [Asterisk](http://asterisk.org).

## Running unit tests

How it is done in the pipeline :
```
export TARGET_VERSION= # Replace with the version you want (in TARGET_VERSION)
./docker_build.sh
```

How you can do it during development :  
(using debian-builder, check here https://gitlab.com/xivo.solutions/debian-builder)
```
debuilder luna
tox
```
