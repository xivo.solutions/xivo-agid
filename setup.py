#!/usr/bin/python3

from setuptools import setup

setup(name='xivo_agid',
      version='1.0',
      description='XIVO AGI Daemon',
      author='Avencall',
      author_email='dev@xivo.solutions',
      url='http://xivo.solutions/',
      packages=['xivo_agid',
                'xivo_agid.bin',
                'xivo_agid.modules',
                'xivo_agid.handlers'],
      scripts=['sbin/xivo-agid']
      )
