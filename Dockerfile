## Image to build from sources

FROM python:3.11-slim-bookworm
LABEL com.avencall.authors="dev@avencall.com"

ENV DEBIAN_FRONTEND noninteractive
ENV HOME /root

# Add dependencies
RUN apt-get update \
    && apt-get install -y --no-install-recommends --auto-remove \
    cups-client \
    curl \
    file \
    git \
    jq \
    libtiff-tools \
    msmtp \
    mutt \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

# Install xivo-agid
WORKDIR /usr/src/agid

COPY requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt
RUN adduser --disabled-password --gecos '' asterisk

COPY setup.py setup.py
COPY xivo_agid/ xivo_agid/
COPY sbin/ sbin/

RUN pip install .

RUN mkdir /var/run/xivo-agid
RUN mkdir -p /etc/xivo/apns

COPY etc/xivo-agid/* /etc/xivo-agid/
COPY docker/010-mds.yml.tmp /etc/xivo-agid/conf.d/
COPY docker/docker-entrypoint.sh /docker-entrypoint.sh
COPY docker/docker-entrypoint.d/ /docker-entrypoint.d/
COPY docker/healthcheck.py /healthcheck.py

WORKDIR /root
# Clean source dir
RUN rm -rf /usr/src/agid

# Version
ARG TARGET_VERSION
LABEL version=${TARGET_VERSION}

EXPOSE 4573
ENTRYPOINT ["/docker-entrypoint.sh"]
CMD ["xivo-agid"]

HEALTHCHECK --timeout=5s --start-period=10s CMD python /healthcheck.py
