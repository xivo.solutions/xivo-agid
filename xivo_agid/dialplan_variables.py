# -*- coding: utf-8 -*-

# Copyright (C) 2006-2014 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

CALL_OPTIONS = 'XIVO_CALLOPTIONS'
CALL_ORIGIN = 'XIVO_CALLORIGIN'
CALL_RECORD_FILE_NAME = 'XIVO_CALLRECORDFILE'

# ID of the callee
EXTEN = 'EXTEN'
SRCNUM = 'XIVO_SRCNUM'
DESTINATION_ID = 'XIVO_DSTID'
DESTINATION_NUMBER = 'XIVO_DSTNUM'
BASE_CONTEXT = 'XIVO_BASE_CONTEXT'
BASE_EXTEN = 'XIVO_BASE_EXTEN'
HANGUP_RING_TIME = 'XIVO_HANGUPRINGTIME'
INTERFACE = 'XIVO_INTERFACE'
OUTCALL_ID = 'XIVO_OUTCALLID'
OUTCALL_PREPROCESS_SUBROUTINE = 'XIVO_OUTCALLPREPROCESS_SUBROUTINE'
PATH = 'XIVO_PATH'
PATH_ID = 'XIVO_PATH_ID'
SOURCE_NUMBER = 'XIVO_SRCNUM'
TRUNK_EXTEN = 'XIVO_TRUNKEXTEN'
TRUNK_SUFFIX = 'XIVO_TRUNKSUFFIX'
FWD_REFERER = 'XIVO_FWD_REFERER'

# ID of the caller
USERID = 'XIVO_USERID'
TRANSFERERNAME = 'TRANSFERERNAME'
FORWARDERNAME = 'FORWARDERNAME'
CALLFORWARDED = 'XIVO_CALLFORWARDED'
REDIRECTING_REASON = 'XIVO_REDIRECTING_REASON'
SIPTRANSFER = 'SIPTRANSFER'
BLINDTRANSFER = 'BLINDTRANSFER'
ATTENDEDTRANSFER = 'ATTENDEDTRANSFER'

# MDS
SIPDOMAIN = 'SIPDOMAIN'
XDS_PEER = 'XDS_PEER'
XDS_CALLEDNUMBER = 'XDS_CALLEDNUMBER'
XDS_CONTEXT = 'XDS_CONTEXT'

# SIP DRIVER
SIPDRIVER = 'XIVO_SIPDRV'

#
MOBILE_PUSH_TOKEN = 'XIVO_MOBILE_PUSH_TOKEN'  # name of mobile push token variable in dialplan
USER_HAS_MOBILE_APP = 'XIVO_USER_HAS_MOBILE_APP'
RING_DEVICE = 'XIVO_PREFERRED_DEVICE'  # name of dialplan variable containing device to ring
