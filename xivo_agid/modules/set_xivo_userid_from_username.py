# -*- coding: utf-8 -*-

# Copyright (C) 2024 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

import logging

from xivo_dao import user_dao

from xivo_agid import agid

logger = logging.getLogger(__name__)

def _set_xivo_userid_from_username(agi, username):
    try:
        user = user_dao.get_user_by_username(username)
        agi.set_variable('XIVO_USERID', user.id)
    except LookupError:
        agi.verbose(f"Warning: No user found for username '{username}'")

def set_xivo_userid_from_username(agi, cursor, args):
    username = args[0] if args and len(args) >= 1 else None

    if not username:
        agi.verbose("Error: No username given cannot set userid")
        return

    try:
        _set_xivo_userid_from_username(agi, username)
    except Exception as e:
        logger.error('Error during set_xivo_userid_from_username: %s', e)
        agi.dp_break(e)

agid.register(set_xivo_userid_from_username)
