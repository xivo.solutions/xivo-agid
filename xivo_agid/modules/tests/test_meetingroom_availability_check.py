# -*- coding: utf-8 -*-

# Copyright (C) 2021 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>
import json
import unittest

from mock import Mock, patch, call

from xivo_agid.modules import meetingroom_availability_check


class TestSetDialplanVars(unittest.TestCase):

    def setUp(self):
        self.queue = Mock()
        self.cursor = Mock()
        self._client = Mock().return_value

    @patch('requests.get')
    def test_start_music_on_hold(self, mock_get):
        def get_variable(value):
            if value == "XIVO_MEETING_ROOM":
                return 'myConfRoom1'
            if value == "SIPPEER(xivo-jitsi)":
                return "1.2.3.4"
            if value == "XIVO_SIPDRV":
                return "SIP"

        agi = Mock()
        agi.get_variable = Mock(side_effect=get_variable)

        agi.config = {'confd': {'client': self._client}, }

        mockresponse = Mock()
        mock_get.return_value = mockresponse

        mockresponse.status_code = 200
        mockresponse.content = json.dumps({
            'participants': 1})

        meetingroom_availability_check.meetingroom_availability_check(agi, self.cursor, None)

        self.assertEqual(agi.execute.call_args_list, [call('SET MUSIC ON')])

    @patch('requests.get')
    def test_join_room_sip(self, mock_get):
        def get_variable(value):
            if value == "XIVO_MEETING_ROOM":
                return 'myConfRoom1'
            if value == "SIPPEER(xivo-jitsi)":
                return "1.2.3.4"
            if value == "XIVO_SIPDRV":
                return "SIP"

        agi = Mock()
        agi.get_variable = Mock(side_effect=get_variable)

        agi.config = {'confd': {'client': self._client}, }

        mockresponse = Mock()
        mock_get.return_value = mockresponse

        mockresponse.status_code = 200
        mockresponse.content = json.dumps({
            'participants': 1})

        expected_calls = [
            call('XIVO_MEETING_ROOM_READY', 0),
            call('XIVO_MEETING_ROOM_READY', 1)
        ]

        meetingroom_availability_check.meetingroom_availability_check(agi, self.cursor, None)

        mock_get.assert_called_once_with('http://1.2.3.4:5280/room-size?room=myConfRoom1&domain=meet.jitsi',
                                         headers={'Accept': 'application/json'},
                                         timeout=1.25)

        self.assertEqual(agi.execute.call_args_list, [call('SET MUSIC ON')])
        self.assertEqual(agi.set_variable.call_args_list, expected_calls)

    @patch('requests.get')
    def test_join_room_pjsip(self, mock_get):
        contact = "sip:xivo-jitsi@192.168.56.112:5060;registering_acc=192_168_56_101"
        uri = "sip:xivo-jitsi@192.168.56.112:5060"
        host = "1.2.3.4"

        def get_variable(value):
            if value == "XIVO_MEETING_ROOM":
                return 'myConfRoom1'
            if value == "XIVO_SIPDRV":
                return "PJSIP"
            if value == "PJSIP_AOR(xivo-jitsi,contact)":
                return contact
            if value == f"PJSIP_CONTACT({contact},uri)":
                return uri
            if value == f"PJSIP_PARSE_URI({uri},host)":
                return host

        agi = Mock()
        agi.get_variable = Mock(side_effect=get_variable)

        agi.config = {'confd': {'client': self._client}, }

        mockresponse = Mock()
        mock_get.return_value = mockresponse

        mockresponse.status_code = 200
        mockresponse.content = json.dumps({
            'participants': 1})

        expected_calls = [
            call('XIVO_MEETING_ROOM_READY', 0),
            call('XIVO_MEETING_ROOM_READY', 1)
        ]

        meetingroom_availability_check.meetingroom_availability_check(agi, self.cursor, None)

        mock_get.assert_called_once_with('http://1.2.3.4:5280/room-size?room=myConfRoom1&domain=meet.jitsi',
                                         headers={'Accept': 'application/json'},
                                         timeout=1.25)

        self.assertEqual(agi.execute.call_args_list, [call('SET MUSIC ON')])
        self.assertEqual(agi.set_variable.call_args_list, expected_calls)

    @patch('requests.get')
    def test_not_join_room(self, mock_get):
        def get_variable(value):
            if value == "XIVO_MEETING_ROOM":
                return 'myConfRoom1'
            if value == "SIPPEER(xivo-jitsi)":
                return "1.2.3.4"
            if value == "XIVO_SIPDRV":
                return "SIP"

        agi = Mock()
        agi.get_variable = Mock(side_effect=get_variable)

        agi.config = {'confd': {'client': self._client}, }

        mockresponse = Mock()
        mock_get.return_value = mockresponse

        mockresponse.status_code = 200
        mockresponse.content = json.dumps({
            'participants': 0})

        expected_calls = [
            call('XIVO_MEETING_ROOM_READY', 0),
            call('XIVO_MEETING_ROOM_READY', 0)
        ]

        meetingroom_availability_check.meetingroom_availability_check(agi, self.cursor, None)

        mock_get.assert_called_once_with('http://1.2.3.4:5280/room-size?room=myConfRoom1&domain=meet.jitsi',
                                         headers={'Accept': 'application/json'},
                                         timeout=1.25)

        self.assertEqual(agi.set_variable.call_args_list, expected_calls)

    @patch('requests.get')
    def test_room_not_created(self, mock_get):
        def get_variable(value):
            if value == "XIVO_MEETING_ROOM":
                return 'myConfRoom1'
            if value == "SIPPEER(xivo-jitsi)":
                return "1.2.3.4"
            if value == "XIVO_SIPDRV":
                return "SIP"

        agi = Mock()
        agi.get_variable = Mock(side_effect=get_variable)

        agi.config = {'confd': {'client': self._client}, }

        mockresponse = Mock()
        mock_get.return_value = mockresponse

        mockresponse.status_code = 404

        expected_calls = [
            call('XIVO_MEETING_ROOM_READY', 0),
            call('XIVO_MEETING_ROOM_READY', 0)
        ]

        meetingroom_availability_check.meetingroom_availability_check(agi, self.cursor, None)

        mock_get.assert_called_once_with('http://1.2.3.4:5280/room-size?room=myConfRoom1&domain=meet.jitsi',
                                         headers={'Accept': 'application/json'},
                                         timeout=1.25)

        self.assertEqual(agi.set_variable.call_args_list, expected_calls)

    @patch('requests.get')
    def test_remote_host_not_found(self, mock_get):
        def get_variable(value):
            if value == "XIVO_MEETING_ROOM":
                return 'myConfRoom1'
            if value == "XIVO_SIPDRV":
                return "SIP"

        agi = Mock()
        agi.get_variable = Mock(side_effect=get_variable)

        agi.config = {'confd': {'client': self._client}, }

        mockresponse = Mock()
        mock_get.return_value = mockresponse

        mockresponse.status_code = 200
        mockresponse.content = json.dumps({
            'participants': 1})

        meetingroom_availability_check.meetingroom_availability_check(agi, self.cursor, None)

        self.assertEqual(agi.execute.call_args_list, [call('SET MUSIC ON')])
        self.assertEqual(agi.appexec.call_args_list, [call('Goto', 'meetingrooms-failure,s,1')])
