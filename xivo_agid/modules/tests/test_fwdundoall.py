# -*- coding: utf-8 -*-

# Copyright (C) 2016 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

import unittest

from mock import Mock, patch

from xivo_agid.modules.fwdundoall import fwdundoall


class TestFwdUndoAll(unittest.TestCase):

    def setUp(self):
        self._user_id = 2
        self._client = Mock().return_value
        self._agi = Mock()
        self._agi.config = {'confd': {'client': self._client},
                            'configmgt': {'token': 'abcdefgh', 'host': 'localhost', 'port': 9100}}
        self._agi.get_variable.return_value = self._user_id

    @patch('requests.put')
    def test_that_fwdundoall_call_confd(self, mock_put):
        mockresponseput = Mock()
        mock_put.return_value = mockresponseput
        mockresponseput.status_code = 200

        fwdundoall(self._agi, None, None)

        disabled = {'enabled': False, 'destination': ''}
        expected_body = {'busy': disabled,
                         'noanswer': disabled,
                         'unconditional': disabled}

        mock_put.assert_called_once_with('http://localhost:9100/configmgt/api/2.0/users/2/services',
                                         headers={'Accept': 'application/json', 'X-Auth-Token': 'abcdefgh'},
                                         timeout=1.25,
                                         json=expected_body)
