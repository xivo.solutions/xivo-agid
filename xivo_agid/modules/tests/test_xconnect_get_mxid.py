# -*- coding: utf-8 -*-

# Copyright (C) 2021 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>
import json
import unittest

from mock import Mock, patch, call

from xivo_agid.modules import xconnect_get_mxid


class TestSetDialplanVars(unittest.TestCase):

    def setUp(self):
        self.queue = Mock()
        self.cursor = Mock()
        self._client = Mock().return_value
     
    @patch('os.getenv', Mock(return_value='default'))
    @patch('requests.get')
    def test_set_mxid_from_api_response_on_main(self, mock_get):

        agi = Mock()
        agi.config = {'xconnect': {'host': 'xconnect.dev.xc', 'secret': 'qwertyuiop'} }

        mockresponse = Mock()
        mock_get.return_value = mockresponse

        mockresponse.status_code = 200
        mockresponse.content = json.dumps({
            'mxid': 'id-ysy8resftx1i', 'phone_number': 1234})
        
        xconnect_get_mxid.xconnect_get_mxid(agi, self.cursor, ["jbond"])
        
        mock_get.assert_called_once_with(
            "https://xconnect.dev.xc/_synapse/client/xivo/users/xivo-username=jbond",
            headers={
                "Accept": "application/json",
                "Authorization": "Bearer qwertyuiop",
            },
            timeout=0.5,
        )
        self.assertEqual(agi.appexec.call_args_list, [call('Goto', 'dial_via_xconnect,1')])
        self.assertEqual(agi.set_variable.call_args_list, [call('XCONNECT_MXID', 'id-ysy8resftx1i')])
    
    @patch('os.getenv', Mock(return_value='mds1'))
    @patch('requests.get')
    def test_set_mxid_from_api_response_on_mds1(self, mock_get):

        agi = Mock()
        agi.config = {'xconnect': {'host': 'xconnect.dev.xc', 'secret': 'qwertyuiop'} }

        mockresponse = Mock()
        mock_get.return_value = mockresponse

        mockresponse.status_code = 200
        mockresponse.content = json.dumps({
            'mxid': 'id-ysy8resftx1i', 'phone_number': 1234})
        xconnect_get_mxid.xconnect_get_mxid(agi, self.cursor, ["jbond"])

        mock_get.assert_called_once_with(
            "https://xconnect.dev.xc/_synapse/client/xivo/users/xivo-username=jbond",
            headers={
                "Accept": "application/json",
                "Authorization": "Bearer qwertyuiop",
            },
            timeout=0.5,
        )

        self.assertEqual(agi.appexec.call_args_list, [call('Goto', 'dial_via_xconnect_via_main,1')])
        self.assertEqual(agi.set_variable.call_args_list, [call('XCONNECT_MXID', 'id-ysy8resftx1i')])


    @patch('requests.get')
    def test_no_mxid_from_api_response(self, mock_get):

        agi = Mock()
        agi.config = {'xconnect': {'host': 'xconnect.dev.xc', 'secret': 'qwertyuiop'} }

        mockresponse = Mock()
        mock_get.return_value = mockresponse

        mockresponse.status_code = 404
        mockresponse.content = None

        xconnect_get_mxid.xconnect_get_mxid(agi, self.cursor, ["jbond"])

        mock_get.assert_called_once_with(
            "https://xconnect.dev.xc/_synapse/client/xivo/users/xivo-username=jbond",
            headers={
                "Accept": "application/json",
                "Authorization": "Bearer qwertyuiop",
            },
            timeout=0.5,
        )
        self.assertEqual(agi.appexec.call_args_list, [call("Goto", "no_user_found,1")])

    def test_missing_arg(self):

        agi = Mock()
        agi.config = {'xconnect': {'host': 'xconnect.dev.xc', 'secret': 'qwertyuiop'} }

        xconnect_get_mxid.xconnect_get_mxid(agi, self.cursor, [])

        self.assertEqual(agi.appexec.call_args_list, [call("Goto", "missing_arg,1")])

    @patch('requests.get')
    def test_error_from_api_response(self, mock_get):

        agi = Mock()
        agi.config = {'xconnect': {'host': 'xconnect.dev.xc', 'secret': 'qwertyuiop'} }

        mockresponse = Mock()
        mock_get.return_value = mockresponse

        mockresponse.status_code = 500
        mockresponse.content = None

        xconnect_get_mxid.xconnect_get_mxid(agi, self.cursor, ["jbond"])

        self.assertEqual(agi.appexec.call_args_list, [call("Goto", "request_error,1")])
