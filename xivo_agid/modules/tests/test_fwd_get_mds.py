# -*- coding: utf-8 -*-

# Copyright (C) 2019 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

import unittest

from mock import Mock, patch

from xivo_agid.modules.fwd_get_mds import fwd_get_mds


class TestFwdGetMds(unittest.TestCase):

    def setUp(self):
        self._agi = Mock()
        self._cursor = Mock()
        self._vars = {}
        self._args = Mock()

        self._agi.get_variable.side_effect = \
            lambda x: self._vars.get(x, '')

        self._agi.set_variable.side_effect = \
            lambda x, y: self._vars.update({x: y})

    def test_fwd_get_mds_queue(self):
        self._vars['XIVO_FWD_ACTION'] = 'queue'
        self._vars['XIVO_FWD_ACTIONARG1'] = '123'

        fwd_get_mds(self._agi, self._cursor, self._args)

        self.assertEqual('XDS_PEER' in self._vars, False)

    def test_fwd_get_mds_meetme(self):
        self._vars['XIVO_FWD_ACTION'] = 'meetme'
        self._vars['XIVO_FWD_ACTIONARG1'] = '123'

        fwd_get_mds(self._agi, self._cursor, self._args)
        self.assertEqual('XDS_PEER' in self._vars, False)

    @patch('os.getenv', Mock(return_value='mds1'))
    def test_fwd_get_mds_queue_mds(self):
        self._vars['XIVO_FWD_ACTION'] = 'queue'
        self._vars['XIVO_FWD_ACTIONARG1'] = '123'

        fwd_get_mds(self._agi, self._cursor, self._args)
        self.assertEqual('XDS_PEER' in self._vars, True)
        self.assertEqual(self._vars['XDS_PEER'], 'default')

    @patch('os.getenv', Mock(return_value='mds1'))
    def test_fwd_get_mds_meetme_mds(self):
        self._vars['XIVO_FWD_ACTION'] = 'meetme'
        self._vars['XIVO_FWD_ACTIONARG1'] = '123'

        fwd_get_mds(self._agi, self._cursor, self._args)
        self.assertEqual('XDS_PEER' in self._vars, True)
        self.assertEqual(self._vars['XDS_PEER'], 'default')

    @patch('os.getenv', Mock(return_value='mds1'))
    @patch('xivo_dao.user_dao.get_mds_by_user_id', Mock(return_value='mds1'))
    def test_fwd_get_mds_user_local(self):
        self._vars['XIVO_FWD_ACTION'] = 'user'
        self._vars['XIVO_FWD_ACTIONARG1'] = '123'

        fwd_get_mds(self._agi, self._cursor, self._args)
        self.assertEqual('XDS_PEER' in self._vars, False)

    @patch('os.getenv', Mock(return_value='mds1'))
    @patch('xivo_dao.user_dao.get_mds_by_user_id', Mock(return_value='mds2'))
    def test_fwd_get_mds_user_remote(self):
        self._vars['XIVO_FWD_ACTION'] = 'user'
        self._vars['XIVO_FWD_ACTIONARG1'] = '123'

        fwd_get_mds(self._agi, self._cursor, self._args)
        self.assertEqual('XDS_PEER' in self._vars, True)
        self.assertEqual(self._vars['XDS_PEER'], 'mds2')

    @patch('os.getenv', Mock(return_value='mds1'))
    @patch('xivo_dao.group_dao.get_mds_by_group_id', Mock(return_value='mds1'))
    def test_fwd_get_mds_group_local(self):
        self._vars['XIVO_FWD_ACTION'] = 'group'
        self._vars['XIVO_FWD_ACTIONARG1'] = '123'

        fwd_get_mds(self._agi, self._cursor, self._args)
        self.assertEqual('XDS_PEER' in self._vars, False)

    @patch('os.getenv', Mock(return_value='mds1'))
    @patch('xivo_dao.group_dao.get_mds_by_group_id', Mock(return_value='mds2'))
    def test_fwd_get_mds_group_remote(self):
        self._vars['XIVO_FWD_ACTION'] = 'group'
        self._vars['XIVO_FWD_ACTIONARG1'] = '123'

        fwd_get_mds(self._agi, self._cursor, self._args)
        self.assertEqual('XDS_PEER' in self._vars, True)
        self.assertEqual(self._vars['XDS_PEER'], 'mds2')
