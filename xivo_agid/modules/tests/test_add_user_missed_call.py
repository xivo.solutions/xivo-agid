# -*- coding: utf-8 -*-

# Copyright (C) 2022 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

import json
import unittest

from mock import Mock, patch, call

from xivo_agid.modules import add_user_missed_call

class TestCallConfigmgtAPI(unittest.TestCase):

    def setUp(self):
        self.cursor = Mock()
        self._client = Mock().return_value

    @patch('requests.get')
    def test_add_missed_call(self, mock_get):
        agi = Mock()

        agi.config = {'confd': {'client': self._client},
                      'configmgt': {'token': 'abcdefgh', 'host': 'localhost', 'port': 9100}}
        mockresponse = Mock()
        mock_get.return_value = mockresponse
        mockresponse.status_code = 200
        mockresponse.content = json.dumps(
            {
                'userId': 1,
                'nbMissedCall': '42'
            }
        )
        add_user_missed_call.add_missed_call_for_user(agi, self.cursor, ["1"])

        mock_get.assert_called_once_with('http://localhost:9100/configmgt/api/2.0/users/1/new_missed_call',
                                         headers={'Accept': 'application/json', 'X-Auth-Token': 'abcdefgh'},
                                         timeout=1.25)
