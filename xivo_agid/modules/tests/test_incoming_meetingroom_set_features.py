# -*- coding: utf-8 -*-

# Copyright (C) 2021 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>
import json
import unittest

from mock import Mock, patch, call

from xivo_agid.modules import incoming_meetingroom_set_features


class TestSetDialplanVars(unittest.TestCase):

    def setUp(self):
        self.queue = Mock()
        self.cursor = Mock()
        self.queue.name = "meeting_room_1"
        self._client = Mock().return_value

    @patch('requests.get')
    def test_get_meeting_room_by_id_sip(self, mock_get):
        def get_variable(value):
            if value == "XIVO_DST_ROOM_ID":
                return '1'
            if value == "XIVO_DST_ROOM_NUM":
                return ''
            if value == "SIPPEER(xivo-jitsi)":
                return "1.2.3.4"
            if value == "XIVO_SIPDRV":
                return "SIP"

        agi = Mock()
        agi.get_variable = Mock(side_effect=get_variable)

        agi.config = {'confd': {'client': self._client},
                      'configmgt': {'token': 'abcdefgh', 'host': 'localhost', 'port': 9100}}

        agi.get_variable.return_value = "1"
        mockresponse = Mock()
        mock_get.return_value = mockresponse
        mockresponse.status_code = 200
        mockresponse.content = json.dumps(
            {
                'id': 1,
                'uuid': '00000000-0000-0000-0000-000000000001',
                'name': 'meeting_room_1',
                'displayName': 'meeting_room_1',
                'userPin': '1234'
            }
        )

        expected_calls = [
            call('XIVO_MEETING_ROOM_TRUNK', 'xivo-jitsi'),
            call('XIVO_MEETING_ROOM_PIN', '1234'),
            call('XIVO_MEETING_ROOM', '00000000-0000-0000-0000-000000000001'),
            call('XIVO_MEETING_ROOM_DISPLAY_NAME', 'meeting_room_1')
        ]

        incoming_meetingroom_set_features.incoming_meetingroom_set_features(agi, self.cursor, None)

        mock_get.assert_called_once_with('http://localhost:9100/configmgt/api/2.0/meetingrooms/static/1',
                                         headers={'Accept': 'application/json', 'X-Auth-Token': 'abcdefgh'},
                                         timeout=1.25)

        self.assertEqual(agi.set_variable.call_args_list, expected_calls)

    @patch('requests.get')
    def test_get_meeting_room_by_id_pjsip(self, mock_get):
        contact = "sip:xivo-jitsi@192.168.56.112:5060;registering_acc=192_168_56_101"
        uri = "sip:xivo-jitsi@192.168.56.112:5060"
        host = "1.2.3.4"

        def get_variable(value):
            if value == "XIVO_DST_ROOM_ID":
                return '1'
            if value == "XIVO_DST_ROOM_NUM":
                return ''
            if value == "XIVO_SIPDRV":
                return "PJSIP"
            if value == "PJSIP_AOR(xivo-jitsi,contact)":
                return contact
            if value == f"PJSIP_CONTACT({contact},uri)":
                return uri
            if value == f"PJSIP_PARSE_URI({uri},host)":
                return host

        agi = Mock()
        agi.get_variable = Mock(side_effect=get_variable)

        agi.config = {'confd': {'client': self._client},
                      'configmgt': {'token': 'abcdefgh', 'host': 'localhost', 'port': 9100}}

        agi.get_variable.return_value = "1"
        mockresponse = Mock()
        mock_get.return_value = mockresponse
        mockresponse.status_code = 200
        mockresponse.content = json.dumps(
            {
                'id': 1,
                'uuid': '00000000-0000-0000-0000-000000000001',
                'name': 'meeting_room_1',
                'displayName': 'meeting_room_1',
                'userPin': '1234'
            }
        )

        expected_calls = [
            call('XIVO_MEETING_ROOM_TRUNK', 'xivo-jitsi'),
            call('XIVO_MEETING_ROOM_PIN', '1234'),
            call('XIVO_MEETING_ROOM', '00000000-0000-0000-0000-000000000001'),
            call('XIVO_MEETING_ROOM_DISPLAY_NAME', 'meeting_room_1')
        ]

        incoming_meetingroom_set_features.incoming_meetingroom_set_features(agi, self.cursor, None)

        mock_get.assert_called_once_with('http://localhost:9100/configmgt/api/2.0/meetingrooms/static/1',
                                         headers={'Accept': 'application/json', 'X-Auth-Token': 'abcdefgh'},
                                         timeout=1.25)

        self.assertEqual(agi.set_variable.call_args_list, expected_calls)

    @patch('requests.get')
    def test_get_meeting_room_by_number_with_pin(self, mock_get):
        def get_variable(value):
            if value == "XIVO_DST_ROOM_ID":
                return ''
            if value == "XIVO_DST_ROOM_NUM":
                return '**1050'
            if value == "SIPPEER(xivo-jitsi)":
                return "1.2.3.4"
            if value == "XIVO_SIPDRV":
                return "SIP"

        agi = Mock()
        agi.get_variable = Mock(side_effect=get_variable)

        agi.config = {'confd': {'client': self._client},
                      'configmgt': {'token': 'abcdefgh', 'host': 'localhost', 'port': 9100}}

        mockresponse = Mock()
        mock_get.return_value = mockresponse
        mockresponse.status_code = 200
        mockresponse.content = json.dumps({
            'uuid': '00000000-0000-0000-0000-000000000000',
            'userPin': '1234',
            'displayName': 'meeting_room_1'})

        expected_calls = [
            call('XIVO_MEETING_ROOM_TRUNK', 'xivo-jitsi'),
            call('XIVO_MEETING_ROOM_PIN', '1234'),
            call('XIVO_MEETING_ROOM', '00000000-0000-0000-0000-000000000000'),
            call('XIVO_MEETING_ROOM_DISPLAY_NAME', 'meeting_room_1')]

        incoming_meetingroom_set_features.incoming_meetingroom_set_features(agi, self.cursor, None)

        mock_get.assert_called_once_with('http://localhost:9100/configmgt/api/2.0/meetingrooms/number/1050',
                                         headers={'Accept': 'application/json', 'X-Auth-Token': 'abcdefgh'},
                                         timeout=1.25)

        agi.set_variable.assert_has_calls(expected_calls)

    @patch('requests.get')
    def test_get_meeting_room_by_number_without_pin(self, mock_get):
        def get_variable(value):
            if value == "XIVO_DST_ROOM_ID":
                return ''
            if value == "XIVO_DST_ROOM_NUM":
                return '**1050'
            if value == "SIPPEER(xivo-jitsi)":
                return "1.2.3.4"
            if value == "XIVO_SIPDRV":
                return "SIP"

        agi = Mock()
        agi.get_variable = Mock(side_effect=get_variable)

        agi.config = {'confd': {'client': self._client},
                      'configmgt': {'token': 'abcdefgh', 'host': 'localhost', 'port': 9100}}

        mockresponse = Mock()
        mock_get.return_value = mockresponse
        mockresponse.status_code = 200
        mockresponse.content = json.dumps({
            'uuid': '00000000-0000-0000-0000-000000000000',
            'displayName': 'meeting_room_1'})

        expected_calls = [
            call('XIVO_MEETING_ROOM_TRUNK', 'xivo-jitsi'),
            call('XIVO_MEETING_ROOM', '00000000-0000-0000-0000-000000000000')]

        incoming_meetingroom_set_features.incoming_meetingroom_set_features(agi, self.cursor, None)

        mock_get.assert_called_once_with('http://localhost:9100/configmgt/api/2.0/meetingrooms/number/1050',
                                         headers={'Accept': 'application/json', 'X-Auth-Token': 'abcdefgh'},
                                         timeout=1.25)

        agi.set_variable.assert_has_calls(expected_calls)

    @patch('requests.get')
    def test_meeting_room_from_mds(self, mock_get):
        def get_variable(value):
            if value == "XIVO_DST_ROOM_ID":
                return ''
            if value == "XIVO_DST_ROOM_NUM":
                return '**1050'

        agi = Mock()
        agi.get_variable = Mock(side_effect=get_variable)

        agi.config = {'confd': {'client': self._client},
                      'configmgt': {'token': 'abcdefgh', 'host': 'localhost', 'port': 9100}}

        mockresponse = Mock()
        mock_get.return_value = mockresponse
        mockresponse.status_code = 200
        mockresponse.content = json.dumps({
            'uuid': '00000000-0000-0000-0000-000000000000',
            'userPin': '1234',
            'displayName': 'meeting_room_1'})

        expected_calls = [
            call('XIVO_MEETING_ROOM', '00000000-0000-0000-0000-000000000000')
        ]

        incoming_meetingroom_set_features.incoming_meetingroom_set_features(agi, self.cursor, None)

        mock_get.assert_called_once_with('http://localhost:9100/configmgt/api/2.0/meetingrooms/number/1050',
                                         headers={'Accept': 'application/json', 'X-Auth-Token': 'abcdefgh'},
                                         timeout=1.25)

        agi.set_variable.assert_has_calls(expected_calls)

    @patch('requests.get')
    def test_get_meeting_room_by_number_does_not_break_when_number_not_found(self, mock_get):

        agi = Mock()

        agi.config = {'confd': {'client': self._client},
                      'configmgt': {'token': 'abcdefgh', 'host': 'localhost', 'port': 9100}}

        mockresponse = Mock()
        mockresponse.status_code = 404
        mock_get.return_value = mockresponse

        expected_calls = [
            call('XIVO_MEETING_ROOM', '')]

        incoming_meetingroom_set_features.get_meeting_room_by_number(agi, '**1050')

        mock_get.assert_called_once_with('http://localhost:9100/configmgt/api/2.0/meetingrooms/number/1050',
                                         headers={'Accept': 'application/json', 'X-Auth-Token': 'abcdefgh'},
                                         timeout=1.25)

        agi.set_variable.assert_has_calls(expected_calls)
