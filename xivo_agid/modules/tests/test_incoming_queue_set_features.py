# -*- coding: utf-8 -*-

# Copyright (C) 2015 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>


import json
import unittest

from mock import Mock, call, patch
from requests import Timeout, ConnectionError

from xivo_agid.modules import incoming_queue_set_features


class TestHoldtimeAnnounce(unittest.TestCase):

    def setUp(self):
        self.agi = Mock()
        self.cursor = Mock()
        self.args = []
        self.queue = Mock()
        self.queue.announce_holdtime = 1
        self.agi.config = { 'configmgt': {'host': 'localhost', 'port': 9100}}

    @patch('xivo_agid.objects.Queue')
    def test_holdtime_use_say_number(self, mock_Queue):
        holdtime_minute = 24
        holdtime_second = holdtime_minute * 60
        self.agi.get_variable.return_value = holdtime_second
        mock_Queue.return_value = self.queue

        incoming_queue_set_features.holdtime_announce(self.agi, self.cursor, self.args)

        self.agi.say_number.assert_called_once_with(str(holdtime_minute), gender='')

    @patch('xivo_agid.objects.Queue')
    def test_holdtime_use_gender_number(self, mock_Queue):
        holdtime_minute = 1
        holdtime_second = holdtime_minute * 60
        self.agi.get_variable.return_value = holdtime_second
        mock_Queue.return_value = self.queue

        incoming_queue_set_features.holdtime_announce(self.agi, self.cursor, self.args)

        self.agi.say_number.assert_called_once_with(str(holdtime_minute), gender='f')

    @patch('requests.get')
    def test_recording_mode_to_apply_ws_is_called(self, mock_Get):
        self.agi.get_variable.return_value = 42

        incoming_queue_set_features.recording_mode_to_apply(self.agi, self.cursor, self.args)

        expected_ws = 'configmgt/api/1.0/recording/mode_to_apply'
        expected_queueid = 42
        mock_Get.assert_called_once_with('http://localhost:9100/%s/%s' % (expected_ws, expected_queueid),
                                         headers={'Accept': 'application/json'}, timeout=1.25)

    @patch('requests.get', side_effect=Timeout())
    def test_recording_mode_to_apply_ws_timeout(self, mock_Get):
        self.agi.get_variable.return_value = 42

        incoming_queue_set_features.recording_mode_to_apply(self.agi, self.cursor, self.args)

        expected_calls = [
            call('XIVO_RECORDING_MODE', 'false'),
        ]
        self.assertEqual(self.agi.set_variable.call_args_list, expected_calls)

    @patch('requests.get', side_effect=ConnectionError())
    def test_recording_mode_to_apply_ws_raises_exception(self, mock_Get):
        self.agi.get_variable.return_value = 42

        incoming_queue_set_features.recording_mode_to_apply(self.agi, self.cursor, self.args)

        expected_calls = [
            call('XIVO_RECORDING_MODE', 'false'),
        ]
        self.assertEqual(self.agi.set_variable.call_args_list, expected_calls)

    @patch('requests.get')
    def test_recording_mode_apply_set_dialplan_var_for_404_status(self, mock_Get):
        tested_status_code = 404

        mocked_GetResponse = mock_Get.return_value
        mocked_GetResponse.status_code = tested_status_code

        incoming_queue_set_features.recording_mode_to_apply(self.agi, self.cursor, self.args)

        expected_calls = [
            call('XIVO_RECORDING_MODE', 'false'),
        ]
        self.assertEqual(self.agi.set_variable.call_args_list, expected_calls)

    @patch('requests.get')
    def test_recording_mode_apply_set_dialplan_var_for_500_status(self, mock_Get):
        tested_status_code = 500

        mocked_GetResponse = mock_Get.return_value
        mocked_GetResponse.status_code = tested_status_code

        incoming_queue_set_features.recording_mode_to_apply(self.agi, self.cursor, self.args)

        expected_calls = [
            call('XIVO_RECORDING_MODE', 'false'),
        ]
        self.assertEqual(self.agi.set_variable.call_args_list, expected_calls)

    @patch('requests.get')
    def test_recording_mode_apply_set_dialplan_var_for_200_status(self, mock_Get):
        tested_status_code = 200
        tested_data = {'mode': 'whatever'}

        mocked_GetResponse = mock_Get.return_value
        mocked_GetResponse.status_code = tested_status_code
        mocked_GetResponse.content = json.dumps(tested_data)

        incoming_queue_set_features.recording_mode_to_apply(self.agi, self.cursor, self.args)

        expected_mode = 'whatever'
        expected_calls = [
            call('XIVO_RECORDING_MODE', expected_mode),
        ]
        self.assertEqual(self.agi.set_variable.call_args_list, expected_calls)
