import unittest
import importlib
from xivo_agid.handlers.userfeatures import UserFeatures

from mock import Mock,patch

mobile_push_notification = importlib.import_module("xivo_agid.modules.mobile_push_notification")

class TestMobilePushNotification(unittest.TestCase):

    def setUp(self):
        def get_variable(value):
            if value == "XIVO_MOBILE_PUSH_TOKEN":
                return str("token")
            if value == "XIVO_SRCNUM":
                return str("1000")
        self.agi = Mock()
        self.agi.get_variable = Mock(side_effect=get_variable)
        mobile_push_notification.INIT_FB=True
        mobile_push_notification.INIT_APNS=True

    @patch('xivo_agid.modules.mobile_push_notification.configuration_notification_firebase_server', return_value={"webpush_config": "webpush_config", "now": "now"})
    @patch('xivo_agid.modules.mobile_push_notification.android_push_notification_server')
    def test_send_message(self, android_push_notification_server, configuration_notification_firebase_server):
        mobile_push_notification.mobile_push_notification(self.agi, None, None, "SIP_callid", True)
        configuration_notification_firebase_server.assert_called()
        android_push_notification_server.assert_called_with(self.agi, "token", "now", "1000", "webpush_config", True)

    @patch('xivo_agid.modules.mobile_push_notification.android_push_notification_server')
    def test_mobile_push_tokens_on_no_firebase(self, mock_android_push):
        self.agi.get_variable.side_effect=['android-token1','1234']
        mobile_push_notification.INIT_FB=False
        mobile_push_notification.mobile_push_notification(self.agi, None, None, "SIP_callid", True)
        mock_android_push.assert_not_called()

    @patch('xivo_agid.modules.mobile_push_notification.android_push_notification_server')
    def test_mobile_push_tokens_on_firebase(self, mock_android_push):
        self.agi.get_variable.side_effect=['android-token1','1234']
        mobile_push_notification.INIT_FB=True
        mobile_push_notification.mobile_push_notification(self.agi, None, None, "SIP_callid", True)
        mock_android_push.assert_called()

    @patch('xivo_agid.modules.mobile_push_notification.ios_push_notification_server')
    def test_mobile_push_tokens_on_no_apns(self, mock_ios_push):
        self.agi.get_variable.side_effect=['ios-token1','1234']
        mobile_push_notification.INIT_APNS=False
        mobile_push_notification.mobile_push_notification(self.agi, None, None, "SIP_callid", True)
        mock_ios_push.assert_not_called()

    @patch('xivo_agid.modules.mobile_push_notification.configuration_notification_apple_server', return_value={"client": "client", "registration_id": "registration_id", "payload": "payload", "config": "config"})
    @patch('xivo_agid.modules.mobile_push_notification.ios_push_notification_server')
    def test_mobile_push_tokens_on_apns(self, mock_ios_push, mock_config_notif):
        self.agi.get_variable.side_effect=['ios-token1','1234']
        mobile_push_notification.INIT_APNS=True
        mobile_push_notification.mobile_push_notification(self.agi, None, None, "SIP_callid", True)
        mock_config_notif.assert_called()
        mock_ios_push.assert_called_with(self.agi, "client", "token1", "payload", "config")

    @patch('xivo_agid.modules.mobile_push_notification.get_server_and_token')
    def test_mobile_push_tokens_on_no_token(self, mock_get_server_and_token):
        self.agi.get_variable.side_effect=[UserFeatures.NOT_REGISTERED,'1234']
        mobile_push_notification.mobile_push_notification(self.agi, None, None, "SIP_callid", True)
        mock_get_server_and_token.assert_not_called()

    def test_split_token_android(self):
        token = "android-bjvrkuebvckluibcrvfe"
        expected_server_token = {"server": "android", "token": "bjvrkuebvckluibcrvfe"}

        result = mobile_push_notification.get_server_and_token(token)
        self.assertEqual(result, expected_server_token)

    def test_split_token_ios(self):
        token = "ios-bjvrkuebvckluibcrvfe"
        expected_server_token = {"server": "ios", "token": "bjvrkuebvckluibcrvfe"}

        result = mobile_push_notification.get_server_and_token(token)
        self.assertEqual(result, expected_server_token)

    def test_split_token_we_dont_know(self):
        token = "bjvrkuebvckluibcrvfe-буки"
        expected_server_token = {"server": "android", "token": "bjvrkuebvckluibcrvfe-буки"}

        result = mobile_push_notification.get_server_and_token(token)
        self.assertEqual(result, expected_server_token)



if __name__ == '__main__':
    unittest.main()
