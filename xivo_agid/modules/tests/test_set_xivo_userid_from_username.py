# -*- coding: utf-8 -*-

# Copyright (C) 2019 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

import unittest

from mock import Mock, patch

from xivo_agid.modules.set_xivo_userid_from_username import set_xivo_userid_from_username
from xivo_dao.alchemy.userfeatures import UserFeatures


class TestsetUseridFromUsername(unittest.TestCase):

    def setUp(self):
        self._agi = Mock()
        self._cursor = Mock()
        self._vars = {}
        self._args = Mock()

    @patch('xivo_agid.modules.set_xivo_userid_from_username.user_dao')
    def test_set_xivo_userid_from_username(self, mock_dao):
        args = ["jbond"]

        mock_dao.get_user_by_username.return_value = UserFeatures(id=1)
        set_xivo_userid_from_username(self._agi, self._cursor, args)

        self._agi.set_variable.assert_called_once_with('XIVO_USERID', 1)

    @patch('xivo_agid.modules.set_xivo_userid_from_username.user_dao')
    def test_dont_set_xivo_userid_from_username_if_none(self, mock_dao):
        args = ["jbond"]

        mock_dao.get_user_by_username.side_effect = LookupError()
        set_xivo_userid_from_username(self._agi, self._cursor, args)

        self._agi.set_variable.assert_not_called()
