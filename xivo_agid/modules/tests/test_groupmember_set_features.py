# -*- coding: utf-8 -*-

# Copyright (C) 2019 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

from typing import Dict
import unittest

from mock import Mock, call, patch
from hamcrest import assert_that
from hamcrest import equal_to
from xivo_agid.modules.groupmember_set_features import groupmember_set_features, extract_call_options


class TestGroupmemberSetFeatures(unittest.TestCase):

    def setUp(self):
        self.agi = Mock()
        self._cursor = Mock()
        self._vars = {}
        self.agi.get_variable.return_value = 'SIP'


    @patch('xivo_agid.objects.User')
    @patch('xivo_agid.objects.Line')
    @patch('xivo_dao.user_line_dao.get_main_exten_by_user_id')
    def test_set_xivo_iface_returns_line_and_webrtc_iface_for_ua_user(self, mock_user_line_dao_get_main_exten_by_user_id, mock_Line, mock_User):
        _args = [1]

        mocked_us = Mock()
        mocked_us.enablednd = False
        mock_User.return_value = mocked_us

        protocol = 'SIP'
        mocked_line = Mock()
        mocked_line.number = '1000'
        mocked_line.name = 'abcd'
        mocked_line.webrtc = 'ua'
        mocked_line.protocol = protocol
        mock_Line.return_value = mocked_line         
        mock_user_line_dao_get_main_exten_by_user_id.return_value = '1000' 
            
        groupmember_set_features(self.agi, self._cursor, _args)

        self.agi.set_variable.assert_any_call('XIVO_INTERFACE', 'SIP/%s' % mocked_line.name)
        self.agi.set_variable.assert_any_call('XIVO_UA_INTERFACE_WEBRTC', 'SIP/%s_w' % mocked_line.name)


    @patch('xivo_agid.objects.User')
    @patch('xivo_agid.objects.Line')
    @patch('xivo_dao.user_line_dao.get_main_exten_by_user_id')
    def test_set_xivo_iface_returns_only_iface_for_nonua_user(self, mock_user_line_dao_get_main_exten_by_user_id, mock_Line, mock_User):
        _args = [1]

        mocked_us = Mock()
        mocked_us.enablednd = False
        mock_User.return_value = mocked_us

        protocol = 'SIP'
        mocked_line = Mock()
        mocked_line.number = '1000'
        mocked_line.name = 'abcd'
        mocked_line.webrtc = 'yes'
        mocked_line.protocol = protocol
        mock_Line.return_value = mocked_line
  
        mock_user_line_dao_get_main_exten_by_user_id.return_value = '1000'
            
        groupmember_set_features(self.agi, self._cursor, _args)

        self.agi.set_variable.assert_any_call('XIVO_INTERFACE', 'SIP/%s' % mocked_line.name)

    @patch('xivo_agid.objects.ExtenFeatures')
    @patch('xivo_agid.objects.User')
    @patch('xivo_agid.objects.Line')
    def test_set_xivo_nofeaturednd(self, mock_Line, mock_User, mock_ExtenFeatures):
        _args = [1]

        mocked_us = Mock()
        mocked_us.enablednd = True
        mock_User.return_value = mocked_us

        mocked_exFeat = Mock()
        mocked_exFeat.enablednd = False
        mock_ExtenFeatures.return_value = mocked_exFeat

        mocked_l = Mock()
        mock_Line.return_value = mocked_l

        groupmember_set_features(self.agi, self._cursor, _args)
        self.agi.set_variable.assert_any_call('XIVO_ENABLEDND', False)   

    @patch('xivo_agid.objects.ExtenFeatures')
    @patch('xivo_agid.objects.User')
    @patch('xivo_agid.objects.Line')
    def test_set_xivo_nouserdnd(self, mock_Line, mock_User, mock_ExtenFeatures):
        _args = [1]

        mocked_us = Mock()
        mocked_us.enablednd = False
        mock_User.return_value = mocked_us

        mocked_exFeat = Mock()
        mocked_exFeat.enablednd = True
        mock_ExtenFeatures.return_value = mocked_exFeat

        mocked_l = Mock()
        mock_Line.return_value = mocked_l
            
        groupmember_set_features(self.agi, self._cursor, _args)
        self.agi.set_variable.assert_any_call('XIVO_ENABLEDND', False)   


    @patch('xivo_agid.objects.ExtenFeatures')
    @patch('xivo_agid.objects.User')
    @patch('xivo_agid.objects.Line')
    def test_set_xivo_enablednd(self, mock_Line, mock_User, mock_ExtenFeatures):
        _args = [1]

        mocked_us = Mock()
        mocked_us.enablednd = True
        mock_User.return_value = mocked_us

        mocked_exFeat = Mock()
        mocked_exFeat.enablednd = True
        mock_ExtenFeatures.return_value = mocked_exFeat

        mocked_l = Mock()
        mock_Line.return_value = mocked_l        
            
        groupmember_set_features(self.agi, self._cursor, _args)

        self.agi.set_variable.assert_any_call('XIVO_ENABLEDND', True)

    def test_that_extract_call_options_keep_valid_option(self):
        queue_options = 'abwdeht'
        result = extract_call_options(queue_options)
        assert_that(result, equal_to('wht'))

    def test_that_extract_call_options_does_not_keep_params_in_parentheses(self):
        queue_options = 'abcdefg(abHh)tij'
        result = extract_call_options(queue_options)
        assert_that(result, equal_to('ti'))

    def test_that_extract_call_options_keep_valid_option(self):
        group_options = 'abwdeht'
        result = extract_call_options(group_options)
        assert_that(result, equal_to('wht'))

    def test_that_extract_call_options_does_not_keep_params_in_parentheses(self):
        group_options = 'abcdefg(abHh)tij'
        result = extract_call_options(group_options)
        assert_that(result, equal_to('ti'))

    @patch('xivo_agid.objects.ExtenFeatures')
    @patch('xivo_agid.objects.User')
    @patch('xivo_agid.objects.Line')
    def test_set_mobile_app_is_false_with_line_with_webrtc_and_mobile_app_info_not_set(self, mock_Line, mock_User, mock_ExtenFeatures):
        _args = [1]    

        mocked_us = Mock()        
        mocked_us.userpreferences = {}
        mock_User.return_value = mocked_us

        mocked_exFeat = Mock()
        mock_ExtenFeatures.return_value = mocked_exFeat

        mocked_line = Mock()
        mocked_line.webrtc = 'yes'
        mock_Line.return_value = mocked_line            
            
        groupmember_set_features(self.agi, self._cursor, _args)

        self.agi.set_variable.assert_any_call('XIVO_USER_HAS_MOBILE_APP', 'False')

    @patch('xivo_agid.objects.ExtenFeatures')
    @patch('xivo_agid.objects.User')
    @patch('xivo_agid.objects.Line')
    def test_set_mobile_app_is_true_with_line_with_webrtc_and_mobile_app_info_set(self, mock_Line, mock_User, mock_ExtenFeatures):
        _args = [1]    

        userpref = {
            "MOBILE_APP_INFO": True
        }

        mocked_us = Mock()        
        mocked_us.userpreferences = userpref
        mock_User.return_value = mocked_us

        mocked_exFeat = Mock()
        mock_ExtenFeatures.return_value = mocked_exFeat

        mocked_line = Mock()
        mocked_line.webrtc = 'yes'
        mock_Line.return_value = mocked_line            
            
        groupmember_set_features(self.agi, self._cursor, _args)

        self.agi.set_variable.assert_any_call('XIVO_USER_HAS_MOBILE_APP', 'True')

    @patch('xivo_agid.objects.ExtenFeatures')
    @patch('xivo_agid.objects.User')
    @patch('xivo_agid.objects.Line')
    def test_set_ring_device_mobile_app_installed(self, mock_Line, mock_User, mock_ExtenFeatures):
        _args = [1]    

        userpref = {
            'MOBILE_APP_INFO': True,
            'PREFERRED_DEVICE': 'smthng'
        }

        mocked_us = Mock()        
        mocked_us.userpreferences = userpref
        mock_User.return_value = mocked_us

        mocked_exFeat = Mock()
        mock_ExtenFeatures.return_value = mocked_exFeat

        mocked_line = Mock()
        mocked_line.webrtc = 'yes'
        mock_Line.return_value = mocked_line            
            
        groupmember_set_features(self.agi, self._cursor, _args)

        self.agi.set_variable.assert_any_call('XIVO_PREFERRED_DEVICE', 'smthng')

    @patch('xivo_agid.objects.ExtenFeatures')
    @patch('xivo_agid.objects.User')
    @patch('xivo_agid.objects.Line')
    def test_set_ring_device_mobile_app_installed_ring_device_not_set(self, mock_Line, mock_User, mock_ExtenFeatures):
        _args = [1]    

        userpref = {
            'MOBILE_APP_INFO': True,
            'PREFERRED_DEVICE': None
        }

        mocked_us = Mock()        
        mocked_us.userpreferences = userpref
        mock_User.return_value = mocked_us

        mocked_exFeat = Mock()
        mock_ExtenFeatures.return_value = mocked_exFeat

        mocked_line = Mock()
        mocked_line.webrtc = 'yes'
        mock_Line.return_value = mocked_line            
            
        groupmember_set_features(self.agi, self._cursor, _args)

        self.agi.set_variable.assert_any_call('XIVO_PREFERRED_DEVICE', '')

    @patch('xivo_agid.objects.ExtenFeatures')
    @patch('xivo_agid.objects.User')
    @patch('xivo_agid.objects.Line')
    def test_set_push_mobile_token_when_mobile_app_is_true(self, mock_Line, mock_User, mock_ExtenFeatures):
        _args = [1]    

        userpref = {
            'MOBILE_APP_INFO': True,
            'PREFERRED_DEVICE': "mobileApp"
        }

        mocked_us = Mock()        
        mocked_us.userpreferences = userpref
        mocked_us.mobile_push_token = "myToken"
        mock_User.return_value = mocked_us

        mocked_exFeat = Mock()
        mock_ExtenFeatures.return_value = mocked_exFeat

        mocked_line = Mock()
        mocked_line.webrtc = 'yes'
        mock_Line.return_value = mocked_line            
            
        groupmember_set_features(self.agi, self._cursor, _args)

        self.agi.set_variable.assert_any_call('XIVO_MOBILE_PUSH_TOKEN', 'myToken')