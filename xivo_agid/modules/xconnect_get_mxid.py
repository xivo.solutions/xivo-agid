# -*- coding: utf-8 -*-

# Copyright (C) 2024 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

import logging
import os
import requests
import json

from xivo_agid import agid

logger = logging.getLogger(__name__)


def get_mxid_from_username(dst_username, agi):
    try:
        response = requests.get(
            "https://%s/_synapse/client/xivo/users/xivo-username=%s"
            % (agi.config["xconnect"]["host"], dst_username),
            headers={
                "Accept": "application/json",
                "Authorization": ("Bearer %s" % agi.config["xconnect"]["secret"]),
            },
            timeout=0.5,
        )
    except requests.Timeout as e:
        agi.verbose('Error requesting connect API (Timeout): %s' % e)
        agi.appexec('Goto', 'request_error,1')
        agi.fail()
    except Exception as e:
        agi.verbose('Error requesting connect API: %s' % e)
        agi.appexec('Goto', 'request_error,1')
        agi.fail()
    else:
        if response.status_code == 200:
            res = json.loads(response.content)
            if res['mxid'] is None:
                agi.appexec('Goto', 'return,1')
            else:
                agi.set_variable('XCONNECT_MXID', res['mxid'])

            systemname = os.getenv('MDS_NAME', 'default')
            if systemname == 'default':
                agi.appexec('Goto', 'dial_via_xconnect,1')
            else:
                agi.appexec('Goto', 'dial_via_xconnect_via_main,1')

        elif response.status_code == 404:
            agi.appexec('Goto', 'no_user_found,1')
        else:
            agi.appexec('Goto', 'request_error,1')
            agi.fail()


def xconnect_get_mxid(agi, cursor, args):
    dst_username = args[0] if args and len(args) >= 1 else None

    if dst_username != None:
        get_mxid_from_username(dst_username, agi)
    else:
        agi.appexec('Goto', 'missing_arg,1')
        agi.fail()

agid.register(xconnect_get_mxid)
